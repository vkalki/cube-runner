﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerCollision : MonoBehaviour
{

    public PlayerMovement playerMovement;
    public Text scoreText;
    private int score;

    //on collision
    public void OnCollisionEnter(Collision collisionInfo)
    {
        //when colliding with an object that has the TAG obstacle
        if (collisionInfo.collider.tag == "ObstacleLeft" || collisionInfo.collider.tag == "ObstacleRight")
        {
            score -= 1;
        }

        if (collisionInfo.collider.tag == "Finish")
        {
            score += 1;
        }

        transform.position = new Vector3(0, 1, -20);
        scoreText.text = "Current Score: " + score;

    }
}
