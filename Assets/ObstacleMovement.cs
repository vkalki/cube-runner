﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    public Vector3 pos1;
    public Vector3 pos2;
    public float speed = 1.0f;


    public void Start() {
        pos1 = new Vector3(-20, transform.position.y, transform.position.z);
        pos2 = new Vector3(20, transform.position.y, transform.position.z);
    }

    public void Update()
    {
        if (tag == "ObstacleLeft")
        {
            transform.position = Vector3.Lerp(pos1, pos2, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
        }
        else if (tag == "ObstacleRight")
        {
            transform.position = Vector3.Lerp(pos2, pos1, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
        }
    }
}
